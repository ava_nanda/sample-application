const express = require("express");
const app = express();
const db = require('./Queries')
const cors = require('cors');
app.use(cors({
    origin:'*'
}))

const PORT = 8040;
app.use(express.json())
app.get('/getdata', db.getdata)
app.post('/insertdata', db.insertdata)


app.listen(PORT, console.log(`Server started on port ${PORT}`));
