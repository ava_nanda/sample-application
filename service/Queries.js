const Pool = require('pg').Pool

const pool = new Pool ({
    user:'postgres',
    host:'localhost',
    database:'sampledb',
    password:'postgres',
    port: 5432
})

const getdata=(req,res)=>
{
    pool.query('select * from People',(err,result)=>
    {
        if(err){
            throw err
        }
        res.status(200).json(result.rows)
    })
}
const insertdata =(req,res)=> {
  
    const {Firstname,lastname}=req.body

    pool.query('insert into People(Firstname,lastname) values($1,$2)',
    [Firstname,lastname],
    (err,result)=>{
        if(err){
            console.log(err)
        }
        res.status(200).send('value added successfully')
    })
}
 
module.exports = {getdata,insertdata};

